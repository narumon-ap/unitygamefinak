﻿using UnityEngine;
using System.Collections;

public class AttackItem : MonoBehaviour {
	Vector3 player;
	public int power;
	public float speed;
	public float timeDestroy;
	float countTime;

	private Vector3 normalizeDirection;
	// Use this for initialization
	void Start () {
		
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>().position;
		countTime = timeDestroy;
		normalizeDirection = (player - transform.position).normalized;
	}
	
	// Update is called once per frame
	void Update () {
		countTime -= Time.deltaTime;
		if(countTime <= 0){
			Destroy(gameObject);
		}else{
			transform.position += normalizeDirection * speed *Time.deltaTime;
		}
	}
	void OnTriggerEnter2D(Collider2D obj){
		
		//If Player
		if(obj.gameObject.CompareTag("Player")){
			obj.gameObject.GetComponent<PlayerController>().decreasePower(power);
			//Decrease power
			Destroy(gameObject);
		}
	}

}
