using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {
	Rigidbody2D rigid2D;
	public Animator animator;
	float jumpForce = 250.0f;
	float walkForce = 30.0f;
	float maxWalkSpeed = 2.0f;

	public float speedBullet = 10.0f;
	public GameObject bulletGameObject;

	Vector3 moveDistance;
	public int hp;
	public int life;

	private int maxHp;
	Vector2 positionShooting;

	public int attackBoss = 100;

	public int MaxHp = 50;
	public AudioClip geet;

	//Touch Screen
	public bool isJump = false;
	public bool isAttack = false;
	public bool isLeft = false;
	public bool isRight = false;

	void Start() {
		rigid2D = GetComponent<Rigidbody2D>();
		this.maxHp = MaxHp;
		GetComponent<AudioSource> ().clip = geet;


	}

	void Update() {
		

		if(Input.GetKeyDown(KeyCode.Space) || isAttack){
			RIGHT ();
		}

		// ��ҵ��ҡ�ҡ ���������������� ��͡�Ѻ�˹�� Scene1 
		if(transform.position.y < -10) {
			SceneManager.LoadScene("Scene1");
		}

		// Shoot with bullet

		if (hp <= 0) {
			SceneManager.LoadScene("GameOverScene");
		}

	}

	public void RIGHT(){

	
			positionShooting = new Vector2 ();
			positionShooting.x = transform.position.x;
			positionShooting.y = transform.position.y + 0.6f;

			if (transform.localScale.x < 0) {
				positionShooting.x += 0.1f;


			}

			Instantiate(bulletGameObject, positionShooting, Quaternion.identity);
			GetComponent<AudioSource> ().Play ();


	}
	void FixedUpdate ()
	{
		// ��ҡ� space 㹢�з����������᡹ y = 0 player �С��ⴴ

		if((Input.GetKeyDown(KeyCode.UpArrow) && rigid2D.velocity.y == 0) || (isJump&& rigid2D.velocity.y == 0)) {
			rigid2D.AddForce(transform.up * jumpForce);
			animator.SetBool ("Walk", false);
		}

		// ����͹价ҧ��ҹ�������ʹ�ҹ��� 
		int key = 0;
		if (Input.GetKey (KeyCode.RightArrow) || isRight) {
			key = 1;
			animator.SetBool ("Walk", true);

		}  // �������١�â�� ����价ҧ���


		if (Input.GetKey (KeyCode.LeftArrow) || isLeft) {
			animator.SetBool ("Walk", true);
			key = -1;

		}   // �������١�ë��� ����价ҧ���� 
		if(key == 0 ){
			animator.SetBool ("Walk", false);
			isLeft = false;
			isRight = false;
			isJump = false;
			isAttack = false;
		}

		// �������Ǣͧ Player
		float speedx = Mathf.Abs(rigid2D.velocity.x);

		//  ��˹����������٧�ش�������͹����� 
		if(speedx < maxWalkSpeed) {
			rigid2D.AddForce(transform.right * key * walkForce);
		}
		// ��Ѻ��ҹ�ҡ����仢�� ���仫��µ����� key 
		if(key != 0) {
			transform.localScale = new Vector3(key, 1, 1);
		}
	}

	public void decreasePower(int attackPower){
		this.hp -= attackPower;
		if(hp <= 0 ){
			//Dead
			hp = 0;
		}


		PlayerPrefs.SetInt("PlayerScore",hp);
	}
	public void IncreasePower(int attackPower){
		hp += attackPower;
		Debug.Log (attackPower);
		if (this.hp > this.maxHp) {
			this.hp = this.maxHp;
		}
		PlayerPrefs.SetInt("PlayerScore",hp);

	}

	public void IncreaseHeart(int attackPower){
		life += attackPower;
		Debug.Log (attackPower);
		PlayerPrefs.SetInt("PlayerScore1",life);


	}
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "doll") // ถ้า GameObject ที่มาชนคือ Player เล่นคลิปเสียงการชน 
		{

			SceneManager.LoadScene("SceneSky");


			// ตรงนี้ต้องปรับเองว่า จะทำลายเร็วหรือช้า ต้องดูความเหมาะสม
		}
		if (other.tag == "doll2") // ถ้า GameObject ที่มาชนคือ Player เล่นคลิปเสียงการชน 
		{
			SceneManager.LoadScene("SceneSea");
			// ตรงนี้ต้องปรับเองว่า จะทำลายเร็วหรือช้า ต้องดูความเหมาะสม
		}
			
		if (other.tag == "doll3") // ถ้า GameObject ที่มาชนคือ Player เล่นคลิปเสียงการชน 
		{
			Debug.Log ("Enter");
			SceneManager.LoadScene("WinScene");
			// ตรงนี้ต้องปรับเองว่า จะทำลายเร็วหรือช้า ต้องดูความเหมาะสม
		}

	}

}