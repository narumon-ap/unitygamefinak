﻿using UnityEngine;
using System.Collections;

public class ArrowGenerator : MonoBehaviour {

	public GameObject arrowPrefab;
	public int begin;
	public int end;

	public float span;
	float delta = 0;


	void Update() {
		delta += Time.deltaTime;
		if(delta > span) {
			delta = 0;
			GameObject arrow = Instantiate(arrowPrefab) as GameObject;
			int px = Random.Range(begin, end);
			arrow.transform.position = new Vector3(px, 8, 0);

			}

	}
}