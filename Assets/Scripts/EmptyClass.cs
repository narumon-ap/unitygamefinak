﻿using UnityEngine;
using System.Collections;

using UnityEngine.SceneManagement;

public class EmptyClass : MonoBehaviour {
	
	public string sceneName;

	void OnTriggerEnter2D(Collider2D obj){

		if (obj.gameObject.CompareTag ("Player")) {

			SceneManager.LoadScene (sceneName);
		}

	}
}