﻿using UnityEngine;
using System.Collections;

public class BulletController : MonoBehaviour {
	Rigidbody2D r2d;
	public float speed = 6;
	Vector3 scale_player;
	// Use this for initialization
	void Start () {
		r2d = GetComponent<Rigidbody2D>();
		scale_player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>().localScale ;
		r2d.velocity = new Vector2(scale_player.x*speed,0);


		if(scale_player.x >= 0){
	
			transform.localScale = new Vector3(1, transform.localScale.y, transform.localScale.z);
		}else{
			
			transform.localScale = new Vector3(-1, transform.localScale.y, transform.localScale.z);
		}
	}

	// Update is called once per frame
	void Update () {



	}
	void FixedUpdate(){
		r2d.velocity.Set(scale_player.x*speed,r2d.velocity.y);
	}
	void OnBecameInvisible() {
		// Destroy the bullet 
		Destroy(gameObject);
	} 
	void OnTriggerEnter2D(Collider2D obj){
		
		if(obj.tag == "Monster"){
			
			Destroy(obj.gameObject);
			Destroy(this.gameObject);
		}
	}
}
