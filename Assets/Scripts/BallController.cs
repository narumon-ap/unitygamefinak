﻿using UnityEngine;
using System.Collections;

public class BallController : MonoBehaviour {

	public int power;
	public AudioClip geet;
	// Use this for initialization
	void Start () {
		GetComponent<AudioSource> ().clip = geet;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D obj){
		//If Player
		if(obj.gameObject.CompareTag("Player")){
			//Decrease power
		
			obj.gameObject.GetComponent<PlayerController>().IncreaseHeart(power);

			GetComponent<AudioSource> ().Play ();
			Destroy(gameObject,geet.length/2);
		}
	}

}
