﻿using UnityEngine;
using System.Collections;

public class MobileControl : MonoBehaviour {
	GameObject obj;
	// Use this for initialization
	void Start () {
		obj = GameObject.FindGameObjectWithTag("Player");

	}
	public void jump(){
		obj.GetComponent<PlayerController>().isJump = true;

	}
	public void left(){
		obj.GetComponent<PlayerController>().isLeft = true;

	}
	public void right(){
		obj.GetComponent<PlayerController>().isRight = true;

	}
	public void attack(){
		obj.GetComponent<PlayerController>().isAttack = true;

	}

	public void jump_Re(){
		obj.GetComponent<PlayerController>().isJump = false;

	}
	public void left_Re(){
		obj.GetComponent<PlayerController>().isLeft = false;

	}
	public void right_Re(){
		obj.GetComponent<PlayerController>().isRight = false;

	}
	public void attack_Re(){
		obj.GetComponent<PlayerController>().isAttack = false;

	}


}
